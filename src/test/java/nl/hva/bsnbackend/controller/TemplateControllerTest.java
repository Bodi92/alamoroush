package nl.hva.bsnbackend.controller;

import nl.hva.bsnbackend.api.controller.TemplateController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TemplateController.class,
        excludeAutoConfiguration = {SecurityAutoConfiguration.class})
class TemplateControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void homePage() throws Exception {
        String pageName = "home";

        mockMvc.perform(MockMvcRequestBuilders.get("/home"))
                .andExpect(status().isOk())
                .andExpect(view().name(pageName));
    }
}
