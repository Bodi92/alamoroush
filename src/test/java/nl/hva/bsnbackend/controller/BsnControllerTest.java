package nl.hva.bsnbackend.controller;

import nl.hva.bsnbackend.api.model.BsnValidationResponseModel;
import nl.hva.bsnbackend.exception.InvalidBsnElevenProofException;
import nl.hva.bsnbackend.exception.InvalidBsnLengthException;
import nl.hva.bsnbackend.persistence.domain.BsnNumber;
import nl.hva.bsnbackend.service.BsnService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(excludeAutoConfiguration = {SecurityAutoConfiguration.class})
class BsnControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private BsnService bsnService;

    private List<BsnNumber> bsnNumberList;

    @BeforeEach
    public void setup() {
        bsnNumberList = new ArrayList<>();
        BsnNumber number1 = new BsnNumber();
        number1.setId(1L);
        number1.setBsn("98983475");

        BsnNumber number2 = new BsnNumber();
        number2.setId(2L);
        number2.setBsn("89678435");

        bsnNumberList.add(number1);
        bsnNumberList.add(number2);
    }

    @Test
    void getAllBsnNums() throws Exception {
        when(bsnService.getAll()).thenReturn(bsnNumberList);

        mockMvc.perform(MockMvcRequestBuilders.get("/bsn")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(jsonPath("$", hasSize(2))).andDo(print());
    }

    @Test
    void checkBsn_Valid() throws Exception {
        String bsn = "674979370";
        BsnValidationResponseModel validResponse = new BsnValidationResponseModel();
        validResponse.setBsn(bsn);
        validResponse.setValid(true);
        validResponse.setCause("BSN is valid.");

        when(bsnService.setValidationResponse(eq(bsn), eq(true), anyString())).thenReturn(validResponse);

        mockMvc.perform(post("/bsn/check/{bsn}", bsn))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bsn").value(bsn))
                .andExpect(jsonPath("$.valid").value(true))
                .andExpect(jsonPath("$.cause").value("BSN is valid."));
    }


    @Test
    void checkBsn_NotValidLength() throws Exception {
        String bsn = "807143";
        InvalidBsnLengthException exception = new InvalidBsnLengthException();

        BsnValidationResponseModel inValidResponse = new BsnValidationResponseModel();
        inValidResponse.setBsn(bsn);
        inValidResponse.setValid(false);
        inValidResponse.setCause(exception.getMessage());

        // Mocking validateBsn to throw InvalidBsnLengthException
        doThrow(exception).when(bsnService).validateBsn(bsn);

        when(bsnService.setValidationResponse(bsn, false, exception.getMessage())).thenReturn(inValidResponse);

        mockMvc.perform(post("/bsn/check/{bsn}", bsn))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.bsn").value(bsn))
                .andExpect(jsonPath("$.valid").value(false))
                .andExpect(jsonPath("$.cause").value(exception.getMessage()));
    }

    @Test
    void checkBsn_NotValidElvenProof() throws Exception {
        String bsn = "807143000";
        InvalidBsnElevenProofException exception = new InvalidBsnElevenProofException();

        BsnValidationResponseModel inValidResponse = new BsnValidationResponseModel();
        inValidResponse.setBsn(bsn);
        inValidResponse.setValid(false);
        inValidResponse.setCause(exception.getMessage());

        // Mocking validateBsn to throw InvalidBsnLengthException
        doThrow(exception).when(bsnService).validateBsn(bsn);

        when(bsnService.setValidationResponse(bsn, false, exception.getMessage())).thenReturn(inValidResponse);

        mockMvc.perform(post("/bsn/check/{bsn}", bsn))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.bsn").value(bsn))
                .andExpect(jsonPath("$.valid").value(false))
                .andExpect(jsonPath("$.cause").value(exception.getMessage()));
    }

}
