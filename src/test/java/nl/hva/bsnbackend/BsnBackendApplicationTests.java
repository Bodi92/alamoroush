package nl.hva.bsnbackend;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class BsnBackendApplicationTests {

    @Autowired
    private BsnBackendApplication bsnBackendApplication;

    @Test
    void contextLoads() {
        assertNotNull(bsnBackendApplication);
    }

}
