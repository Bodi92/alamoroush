package nl.hva.bsnbackend.service;

import nl.hva.bsnbackend.api.model.BsnValidationResponseModel;
import nl.hva.bsnbackend.exception.InvalidBsnElevenProofException;
import nl.hva.bsnbackend.exception.InvalidBsnLengthException;
import nl.hva.bsnbackend.persistence.domain.BsnNumber;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import nl.hva.bsnbackend.persistence.repository.BsnRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class BsnServiceTest {

    @InjectMocks
    private BsnService bsnService;

    @Mock
    private BsnRepository bsnRepository;

    private Logger mockLogger;

    @BeforeEach
    public void setup() {
        // Create a mock logger
        mockLogger = mock(Logger.class);
        BsnService.setLogger(mockLogger);

    }

    @Test
    void addBsnNumber() {
        BsnNumber bsnNumber = new BsnNumber();
        bsnNumber.setBsn("98983475");

        // Mocking the behavior of bsnRepository.save() method to return the same BsnNumber object
        Mockito.when(bsnRepository.save(Mockito.any(BsnNumber.class))).thenReturn(bsnNumber);

        BsnNumber saved = bsnService.addNumber(bsnNumber);

        assertEquals(bsnNumber.getId(), saved.getId());
        assertEquals(bsnNumber.getBsn(), saved.getBsn());
    }

    @Test
    void checkNumberLength_Valid() {
        String bsn8Digit = "97500838";
        String bsn9Digit = "975008380";

        assertTrue(bsnService.isvalidLength(bsn8Digit));
        assertTrue(bsnService.isvalidLength(bsn9Digit));

        // Verify that the log message was generated with the correct arguments
        verify(mockLogger).info("Validation result for BSN '{}' : {}", bsn8Digit, "Valid length");
        verify(mockLogger).info("Validation result for BSN '{}' : {}", bsn9Digit, "Valid length");

    }

    @Test
    void checkNumberLength_Invalid() {
        String bsn7Digit = "7653783";
        String bsn6Digit = "347783";

        assertEquals(false, bsnService.isvalidLength(bsn7Digit));
        assertEquals(false, bsnService.isvalidLength(bsn6Digit));

        // Verify that the log message was generated with the correct arguments
        verify(mockLogger).info("Validation result for BSN '{}' : {}", bsn7Digit, "Invalid length");
        verify(mockLogger).info("Validation result for BSN '{}' : {}", bsn6Digit, "Invalid length");
    }

    @Test
    void testElevenProofValidation_Valid() {
        String validBsn = "240990626";
        String validBsnEndWith0 = "674979370";

        int expectedSumValidBsn = 165;
        int expectedSumValidBsnEndWith0 = 286;

        assertTrue(bsnService.elevenProofValidation(validBsn));
        assertTrue(bsnService.elevenProofValidation(validBsnEndWith0));

        // Verify that the correct log message was generated
        verify(mockLogger).info("Sum of elfproef validation for BSN '{}' : {}", validBsn, expectedSumValidBsn);
        verify(mockLogger).info("Sum of elfproef validation for BSN '{}' : {}", validBsnEndWith0, expectedSumValidBsnEndWith0);

    }

    @Test
    void testElevenProofValidation_Invalid() {
        String notValidBsn = "987654321";
        int expectedSumNotValidBsn = 283;

        assertEquals(false, bsnService.elevenProofValidation(notValidBsn));

        // Verify that the correct log message was generated
        verify(mockLogger).info("Sum of elfproef validation for BSN '{}' : {}", notValidBsn, expectedSumNotValidBsn);
    }

    @Test
    void validateBsn_NonDigitBsn() {
        String nonDigitBsn = "123qwe567";
        assertThrows(IllegalArgumentException.class, () -> {
            bsnService.validateBsn(nonDigitBsn);
        });
    }

    @Test
    void validateBsn_emptyBsn() {
        String emptyBsn = "";
        assertThrows(IllegalArgumentException.class, () -> {
            bsnService.validateBsn(emptyBsn);
        });
    }

    @Test
    void validateBsn_invalidLengthBsn() {
        String invalidLengthBsn = "1234";
        assertThrows(InvalidBsnLengthException.class, () -> {
            bsnService.validateBsn(invalidLengthBsn);
        });
    }

    @Test
    void validateBsn_invalidElevenProofValidationBsn() {
        String invalidElevenProof = "123456789";
        assertThrows(InvalidBsnElevenProofException.class, () -> {
            bsnService.validateBsn(invalidElevenProof);
        });
    }

    @Test
    void setValidationResponse(){
        String bsn = "674979370";
        boolean isValid = true;
        String cause = "BSN is valid.";

        BsnValidationResponseModel responseModel = bsnService.setValidationResponse(bsn, isValid, cause);

        assertNotNull(responseModel);
        assertEquals(bsn, responseModel.getBsn());
        assertEquals(isValid, responseModel.getValid());
        assertEquals(cause, responseModel.getCause());
    }

    @Test
    void getAll(){
        BsnNumber bsnNumber1 = new BsnNumber();
        bsnNumber1.setBsn("150465002");

        BsnNumber bsnNumber2 = new BsnNumber();
        bsnNumber1.setBsn("304766240");

        List<BsnNumber> bsnNumberList = new ArrayList<>();
        bsnNumberList.add(bsnNumber1);
        bsnNumberList.add(bsnNumber2);

        when(bsnRepository.findAll()).thenReturn(bsnNumberList);

        List<BsnNumber> resultList = bsnService.getAll();
        assertEquals(bsnNumberList, resultList);

    }

}
