package nl.hva.bsnbackend.service;

import nl.hva.bsnbackend.api.model.BsnValidationResponseModel;
import nl.hva.bsnbackend.exception.InvalidBsnElevenProofException;
import nl.hva.bsnbackend.exception.InvalidBsnLengthException;
import nl.hva.bsnbackend.persistence.domain.BsnNumber;
import nl.hva.bsnbackend.persistence.repository.BsnRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

@Service
public class BsnService {
    private static Logger logger = LoggerFactory.getLogger(BsnService.class);

    private final BsnRepository bsnRepository;

    public BsnService(BsnRepository bsnRepository) {
        this.bsnRepository = bsnRepository;
    }

    public List<BsnNumber> getAll() {
        return bsnRepository.findAll();
    }

    @Transactional
    public BsnNumber addNumber(BsnNumber bsnNumber) {
        return bsnRepository.save(bsnNumber);
    }

    public Boolean isvalidLength(String bnsNumber) {
        Pattern pattern = Pattern.compile("^\\d{8,9}$");
        Boolean isMatcher = pattern.matcher(bnsNumber).matches();
        logger.info("Validation result for BSN '{}' : {}", bnsNumber, Boolean.TRUE.equals(isMatcher) ? "Valid length" : "Invalid length");
        return isMatcher;
    }

    public Boolean elevenProofValidation(String bsnNumber) {
        int sum = 0;
        for (int i = 0; i < bsnNumber.length() - 1; i++) {
            int num = Character.getNumericValue(bsnNumber.charAt(i));
            sum += num * (9 - i);
        }
        int lastNum = Character.getNumericValue(bsnNumber.charAt(bsnNumber.length() - 1));
        sum -= lastNum;
        logger.info("Sum of elfproef validation for BSN '{}' : {}", bsnNumber, sum);
        return sum % 11 == 0;
    }

    public void validateBsn(String bsn) {
        if (bsn == null || bsn.isEmpty()) {
            throw new IllegalArgumentException("BSN cannot be empty");
        }
        if (!bsn.matches("\\d+")) {
            throw new IllegalArgumentException("BSN must contain only digits");
        }
        if (Boolean.FALSE.equals(isvalidLength(bsn))) {
            throw new InvalidBsnLengthException();
        }
        if (Boolean.FALSE.equals(elevenProofValidation(bsn))) {
            throw new InvalidBsnElevenProofException();
        }
    }

    public BsnValidationResponseModel setValidationResponse(String bsn, boolean isValid, String cause) {
        BsnValidationResponseModel responseModel = new BsnValidationResponseModel();
        responseModel.setBsn(bsn);
        responseModel.setValid(isValid);
        responseModel.setCause(cause);
        return responseModel;
    }

    public static void setLogger(Logger newLogger) {
        logger = newLogger;
    }

}
