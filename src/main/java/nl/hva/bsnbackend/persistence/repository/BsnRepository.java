package nl.hva.bsnbackend.persistence.repository;

import nl.hva.bsnbackend.persistence.domain.BsnNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface BsnRepository extends JpaRepository<BsnNumber, Long> {
}
