package nl.hva.bsnbackend.persistence.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;


@Entity
public class BsnNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String bsn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBsn() {
        return bsn;
    }

    public void setBsn(String bsn) {
        this.bsn = bsn;
    }
}
