package nl.hva.bsnbackend.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/home")
public class TemplateController {

    @GetMapping
    public String homePage() {
        return "home";
    }
}
