package nl.hva.bsnbackend.api.controller;

import nl.hva.bsnbackend.api.model.BsnValidationResponseModel;
import nl.hva.bsnbackend.exception.InvalidBsnElevenProofException;
import nl.hva.bsnbackend.exception.InvalidBsnLengthException;
import nl.hva.bsnbackend.persistence.domain.BsnNumber;
import nl.hva.bsnbackend.service.BsnService;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bsn", produces = MediaTypes.HAL_JSON_VALUE)
public class BsnController {

    private final BsnService bsnService;

    public BsnController(BsnService bsnService) {
        this.bsnService = bsnService;
    }

    @GetMapping
    public ResponseEntity<List<BsnNumber>> getAll() {
        List<BsnNumber> bnsNumbers = bsnService.getAll();
        return ResponseEntity.ok(bnsNumbers);
    }

    @PostMapping("/add")
    public ResponseEntity<BsnNumber> addNumber(@RequestBody BsnNumber bsnNumber) {
        BsnNumber created = bsnService.addNumber(bsnNumber);
        return ResponseEntity.ok(created);
    }

    @PostMapping("/check/{bsn}")
    public ResponseEntity<BsnValidationResponseModel> check(@PathVariable String bsn) {
        BsnValidationResponseModel responseModel;
        try {
            bsnService.validateBsn(bsn);
            responseModel = bsnService.setValidationResponse(bsn, true, "BSN is valid.");
            return ResponseEntity.ok(responseModel);
        } catch (InvalidBsnElevenProofException | InvalidBsnLengthException e) {
            responseModel = bsnService.setValidationResponse(bsn, false, e.getMessage());
            return ResponseEntity.badRequest().body(responseModel);
        } catch (IllegalArgumentException e) {
            responseModel = bsnService.setValidationResponse(bsn, false, "Invalid BSN: " + e.getMessage());
            return ResponseEntity.badRequest().body(responseModel);
        }
    }

}
