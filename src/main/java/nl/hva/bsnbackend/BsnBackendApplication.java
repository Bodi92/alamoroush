package nl.hva.bsnbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsnBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsnBackendApplication.class, args);
    }

}
