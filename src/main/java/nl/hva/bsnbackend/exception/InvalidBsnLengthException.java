package nl.hva.bsnbackend.exception;

public class InvalidBsnLengthException extends RuntimeException {

    public InvalidBsnLengthException() {
        super("BSN length must be exactly 8 or 9 digits.");
    }
}
