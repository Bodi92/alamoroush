package nl.hva.bsnbackend.exception;

public class InvalidBsnElevenProofException extends RuntimeException{

    public InvalidBsnElevenProofException() {
        super("The BSN does not pass the eleven proof validation.");
    }
}
