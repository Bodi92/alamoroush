document.getElementById('validateBtn').addEventListener('click', function () {
    let bsn = document.getElementById('bsnInput').value;
    if (bsn.trim() === '') {
        // Handle empty input
        $('#responseContainer').html('<p> BSN is empty</p>');
        return;
    }
    console.log(bsn);
    fetch(`bsn/check/${bsn}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
        .then(response => {
            $('#responseContainer').html('<p>' + response.cause + '</p>');
        })
        .catch(error => {
            console.error('Error:', error);
        });
});